export default [
  [
    [
      {
        name: 'lifestyle',
        label: 'I\'m a',
        type: 'select',
        options: ['lover', 'hater']
      },
      {
        name: 'age',
        label: 'Age',
        type: 'select',
        options: (() => {
          const out = []
          for (let i = 0; i < 100 - 16; i++) {
            out.push({ label: i + 18, value: i + 18 })
          }
          return out
        })(),
        validate: [
          v => (v && v.value) || 'Your age is required'
        ]
      },
      [
        {
          name: 'id',
          label: 'ID',
          type: 'text',
          attributes: {
            readonly: true
          }
        },
        {
          name: 'uin',
          label: 'UIN',
          type: 'text',
          attributes: {
            readonly: true
          }
        }
      ],
      {
        name: 'fruit',
        label: 'Favourite Fruit',
        type: 'select',
        options: [
          'Apple',
          'Banana',
          'Pear'
        ]
      },
      {
        name: 'location',
        label: 'Location',
        type: 'text'
      }
    ],
    [
      {
        name: 'mood',
        label: 'Mood',
        type: 'text'
      },
      {
        name: 'about',
        label: 'About Me',
        type: 'textarea'
      },
      {
        name: 'something',
        label: 'Something',
        type: 'radio',
        options: (['Rather not say', 'Yep', 'Nope']).map(n => ({
          value: n,
          label: n
        }))
      }
    ]
  ],
  {
    name: 'color',
    label: 'Color',
    type: 'colorpicker',
    required: true,
    options: [
      'blue',
      '#F70BDE',
      '#D58AFB',
      '#97BAFA',
      '#02D1DF',
      '#02E685',
      '#49FBB0',
      '#01FD20',
      '#06E902',
      '#28B463',
      '#FDE142',
      '#E1C004',
      '#FDD069',
      ''
    ]
  }
]
